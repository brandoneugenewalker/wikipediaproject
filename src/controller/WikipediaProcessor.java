package controller;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.TreeMap;

import model.Digraph;

/**
 * This class will process the file containing data about the ID's of wikipedia
 * pages.
 * 
 * @author Brandon Eugene Walker
 *
 * @version Oct 24, 2018
 *
 */
public class WikipediaProcessor {

	private static final int NAME_FIELD = 0;
	private static final int ID_FIELD = 1;
	private static final int ID_TO_ADD_EDGES_TOO = 0;
	public TreeMap<Integer, String> mappedIDAndNames = new TreeMap<Integer, String>();
	public Digraph<Integer> wikipediaGraph = new Digraph<Integer>();

	public void processWikipediaIDFile() {

		Scanner source = null;
		try {
			source = new Scanner(new File("WikipediaIDs.txt"));
		} catch (IOException err) {
			err.printStackTrace();
			System.exit(0);
		}
		while (source.hasNext()) {
			String[] line = source.nextLine().split(":");
			try {
				String currentName = line[WikipediaProcessor.NAME_FIELD].toLowerCase();
				int currentID = Integer.parseInt(line[WikipediaProcessor.ID_FIELD]);
				this.mappedIDAndNames.put(currentID, currentName);
				this.wikipediaGraph.addNode(currentID);
			} catch (NumberFormatException exc) {
				continue;
			} catch (ArrayIndexOutOfBoundsException aioobe) {
				continue;
			}

		}
		source.close();

	}

	public void processWikipediaLinksFile() {
		Scanner source = null;

		try {
			source = new Scanner(new File("WikipediaLinks.txt"));
		} catch (IOException err) {
			err.printStackTrace();
			System.exit(0);
		}
		while (source.hasNext()) {
			String[] line = source.nextLine().split(" ");
			int idToAddTo = Integer.parseInt(line[WikipediaProcessor.ID_TO_ADD_EDGES_TOO]);
			for (int i = 1; i < line.length; i++) {
				try {
					int currentID = Integer.parseInt(line[i]);
					this.wikipediaGraph.addEdge(idToAddTo, currentID);
				} catch (IllegalArgumentException iae) {
					continue;
				} catch (Exception exc) {
					exc.printStackTrace();
					System.exit(1);
				}
			}

		}
		source.close();
	}

}
