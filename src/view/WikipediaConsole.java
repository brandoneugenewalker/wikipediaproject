/**
 * 
 */
package view;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

import controller.WikipediaProcessor;

/**
 * @author Brandon Eugene Walker
 *
 * @version Oct 27, 2018
 *
 */
public class WikipediaConsole {
	private static WikipediaProcessor theProcessor = new WikipediaProcessor();
	private static Scanner scan = null;
	private static Map<Integer, Integer> links = null;

	public static void main(String[] args) {
		scan = new Scanner(System.in);

		System.out.println("Welcome to the Wikipedia Data Application, initializing the data may take some time.");

		System.out.println("Processing Wikipedia ID's...");
		long startTimeID = System.nanoTime();
		theProcessor.processWikipediaIDFile();
		long endTimeID = System.nanoTime();
		long duationID = ((endTimeID - startTimeID) / 1000000000);
		System.out.println("It took " + duationID + " seconds to process the ID's.");

		System.out.println("Processing Wikipedia links...");
		long startTimeLinks = System.nanoTime();
		theProcessor.processWikipediaLinksFile();
		long endTimeLinks = System.nanoTime();
		long durationLinks = ((endTimeLinks - startTimeLinks) / 1000000000);
		System.out.println("It took " + durationLinks + " seconds to process the links.");

		promptUserForTask();

		scan.close();

	}

	private static void promptUserForTask() {
		System.out.println("What would you like to do today?");
		System.out.println(
				"View top 100 most linked to articles <This can only be done once!> [top100] ||| Solve the Wikipedia Link Game [game] ||| Print out all of the ID and Names [names]   ||| Exit [exit]?");
		String answer = scan.nextLine();
		if (answer.equalsIgnoreCase("top100")) {
			System.out.println("The top 100 articles are...");
			buildAndDisplayTop100();
		} else if (answer.equalsIgnoreCase("game")) {
			playWikipediaGame();
		} else if (answer.equalsIgnoreCase("names")) {
			printNames();
		} else if (answer.equalsIgnoreCase("exit")) {
			System.out.println("Goodbye!");
			System.exit(0);
		} else {
			System.out.println("Please input a valid option.");
		}
	}

	private static void buildAndDisplayTop100() {
		long startTime100 = System.nanoTime();
		int count = 0;
		while (count < 100) {
			List<Integer> result = theProcessor.wikipediaGraph.maximalDegreeList();
			for (int currentID : result) {
				String pageName = theProcessor.mappedIDAndNames.get(currentID);
				System.out.println("#" + (count + 1) + " = " + currentID + " : " + pageName);
				theProcessor.wikipediaGraph.remove(currentID);
			}
			count++;
		}
		long endTime100 = System.nanoTime();
		long duration100 = ((endTime100 - startTime100) / 1000000000);
		System.out.println("It took " + duration100 + " seconds to display the top 100 articles.");
		System.out.println("Task complete, program will now exit.");
		System.exit(0);
	}

	private static void playWikipediaGame() {
		int responseID2 = 0;

		if (links == null) {
			System.out.println("Building game map, this may take some time...");
			long startTimeGame = System.nanoTime();
			links = theProcessor.wikipediaGraph.breadthFirstSearchMap(595315);
			long endTimeGame = System.nanoTime();
			long durationGame = ((endTimeGame - startTimeGame) / 1000000000);
			System.out.println("It took " + durationGame + " seconds to build the game map.");
		}

		System.out.println("What node would you like to path to? (ID#)");

		try {
			String response2 = scan.nextLine();
			responseID2 = Integer.parseInt(response2);
		} catch (Exception exc) {
			System.out.println("Please input a valid ID number.");
			playWikipediaGame();
		}

		if (!links.containsKey(responseID2)) {
			System.out.println("That page was not found, please try again.");
			playWikipediaGame();
		}

		boolean isDone = false;
		int currentNode = responseID2;

		while (!isDone) {
			String nodeName = theProcessor.mappedIDAndNames.get(currentNode);
			System.out.println(nodeName + " : " + currentNode);
			currentNode = links.get(currentNode);
			if (currentNode == 595315) {
				isDone = true;
			}
		}

		System.out.println("university of west georgia" + " : " + "595315");

		System.out.println("Would you like to play again? [y/n]");
		String responseYesNo = scan.nextLine();
		if (responseYesNo.equalsIgnoreCase("y")) {
			playWikipediaGame();
		} else {
			promptUserForTask();
		}

	}

	private static void printNames() {
		var IDs = theProcessor.mappedIDAndNames.keySet();
		System.out.println("Wikipedia Pages:" + IDs.size());
		for (int currentID : IDs) {
			System.out.println(theProcessor.mappedIDAndNames.get(currentID) + ":" + currentID);
		}
		promptUserForTask();
	}

}
